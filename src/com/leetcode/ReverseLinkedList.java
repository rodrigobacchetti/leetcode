package com.leetcode;

public class ReverseLinkedList {

    public static void main(String[] args) {
        Element element = new Element("A", new Element("B", new Element("C")));
        printElements(element);
        element = reverse(element);
        printElements(element);
    }

    public static Element reverse(Element element) {
        Element previous = null;
        Element now = element;
        Element next = null;

        while (now != null) {
            next = now.getNext();
            now.setNext(previous);
            previous = now;
            now = next;
        }

        return previous;

    }

    public static void printElements(Element element) {
        while (element != null) {
            System.out.print(element.getValue() + " ");
            element = element.getNext();
        }
    }

    public static class Element {

        private String value;
        private Element next;

        public Element() {
        }

        public Element(String value) {
            this.value = value;
        }

        public Element(String value, Element next) {
            this.value = value;
            this.next = next;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public Element getNext() {
            return next;
        }

        public void setNext(Element next) {
            this.next = next;
        }
    }
}
