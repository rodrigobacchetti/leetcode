package com.leetcode;

public class Fibonacci {

    public static void main(String[] args) {
        int position = 6;
        System.out.println("\nAt position" + position + " Fibonacci is: " + fibonacci(position));
    }

    public static int fibonacci(int n) {
        int n1 = 0;
        int n2 = 1;
        int n3 = 0;

        for (int i = 2; i <= n; i++) {
            n3 = n1 + n2;
            System.out.print(" " + n3);
            n1 = n2;
            n2 = n3;
        }

        return n3;
    }
}
